package io.axual.training.generator;

import io.axual.general.Application;
import io.axual.general.Currency;
import io.axual.payments.AccountEntryEvent;
import io.axual.payments.AccountId;
import io.axual.payments.CreditDebitType;
import io.axual.training.common.Account;
import io.axual.training.common.AmountUtils;
import io.axual.training.common.EventGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Component
@ConfigurationProperties(prefix="generator")
public final class AccountEntryEventGenerator implements EventGenerator<AccountId, AccountEntryEvent> {
    private static final Logger logger = LoggerFactory.getLogger(AccountEntryEventGenerator.class);

    private static final int NUM_ACCOUNTS = 20;
    private static final int MAX_AMOUNT = 750;
    private static final int DECIMALS = 10;
    private final Application application;
    private final Currency currency;
    private List<Account> accounts = Account.getAll();
    private Random random = new Random();
    private AccountEntryEvent nextEvent = null;
    private int txCounter = 0;

    private String applicationVersion;

    public AccountEntryEventGenerator() {
        application = Application.newBuilder()
                .setName(getClass().getName())
                .setVersion(applicationVersion)
                .build();
        currency = Currency.newBuilder()
                .setSymbol("EUR")
                .build();
    }

    private AccountEntryEvent[] generateTransaction() {
        // Select the amount to transfer
        StringBuilder amountBuilder = new StringBuilder();
        amountBuilder.append(random.nextInt(MAX_AMOUNT));
        amountBuilder.append(".");
        amountBuilder.append(random.nextInt(DECIMALS)).append(random.nextInt(DECIMALS));
        BigDecimal amount = new BigDecimal(amountBuilder.toString());

        // Find an account that can transfer the amount
        Account fromAccount = accounts.get(random.nextInt(NUM_ACCOUNTS));
        while (!fromAccount.canDebitAmount(amount)) {
            fromAccount = accounts.get(random.nextInt(NUM_ACCOUNTS));
        }

        // Find another account to transfer to
        Account toAccount = accounts.get(random.nextInt(NUM_ACCOUNTS));
        while (toAccount.equals(fromAccount) || !toAccount.canCreditAmount(amount)) {
            toAccount = accounts.get(random.nextInt(NUM_ACCOUNTS));
        }

        // Transfer the amount
        BigDecimal fromBefore = fromAccount.getBalance();
        BigDecimal toBefore = toAccount.getBalance();

        BigDecimal fromAfter = fromBefore.subtract(amount);
        BigDecimal toAfter = toBefore.add(amount);

        fromAccount.setBalance(fromAfter);
        toAccount.setBalance(toAfter);

        // Generate the proper account entries
        AccountEntryEvent event1 = AccountEntryEvent.newBuilder()
                .setTimestamp(System.currentTimeMillis())
                .setSource(application)
                .setKey(UUID.randomUUID().toString())
                .setAccount(fromAccount.getIdentification())
                .setCurrency(currency)
                .setAmount(AmountUtils.createAmount(amount))
                .setBalanceAfterTransaction(AmountUtils.createAmount(fromAfter))
                .setCreditDebitIndicator(CreditDebitType.DEBIT)
                .setDescription("Transaction " + ++txCounter)
                .setCounterpartyAccount(toAccount.getIdentification())
                .setCounterpartyName(toAccount.getName())
                .build();

        AccountEntryEvent event2 = AccountEntryEvent.newBuilder()
                .setTimestamp(event1.getTimestamp())
                .setSource(application)
                .setKey(UUID.randomUUID().toString())
                .setAccount(toAccount.getIdentification())
                .setCurrency(currency)
                .setAmount(event1.getAmount())
                .setBalanceAfterTransaction(AmountUtils.createAmount(toAfter))
                .setCreditDebitIndicator(CreditDebitType.CREDIT)
                .setDescription(event1.getDescription())
                .setCounterpartyAccount(fromAccount.getIdentification())
                .setCounterpartyName(fromAccount.getName())
                .build();

        return new AccountEntryEvent[]{event1, event2};
    }

    public AccountEntryEvent generate() {
        AccountEntryEvent event;
        if (nextEvent == null) {
            AccountEntryEvent[] events = generateTransaction();
            event = events[0];
            nextEvent = events[1];
        } else {
            event = nextEvent;
            nextEvent = null;
        }
        logger.info("Generated AccountEntryEvent (fromIBAN={},toIBAN={},{} amount={})",event.getAccount().getId(),event.getCounterpartyAccount().getId(),event.getCurrency().getSymbol(),event.getAmount().getValue());
        return event;
    }

    @Override
    public AccountId getKey(AccountEntryEvent record) {
        return record.getAccount();
    }

    public void setApplicationVersion(String applicationVersion) {
        this.applicationVersion = applicationVersion;
    }
}
