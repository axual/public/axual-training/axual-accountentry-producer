# AccountEntry producer application
This app generates random accountentries representing transfers from account A to account B.

Below you see an example of an accountentry event:

``` 
{
	"timestamp": 1516819524407,
	"source": {
		"name": "io.axual.training.generator.AccountEntryEventGenerator",
		"version": null
	},
	"key": "8ae4ba32-63a7-4381-9684-e75a36aadc59",
	"account": {
		"id": {
			"io.axual.payments.IBAN": {
				"accountNumber": "NL08RABO0000000008"
			}
		}
	},
	"currency": {
		"symbol": "EUR"
	},
	"amount": {
		"value": "0.20"
	},
	"balanceAfterTransaction": {
		"value": "1656.14"
	},
	"creditDebitIndicator": "DEBIT",
	"description": "Transaction 5",
	"counterpartyAccount": {
		"id": {
			"io.axual.payments.IBAN": {
				"accountNumber": "NL01RABO0000000001"
			}
		}
	},
	"counterpartyName": "Account #1"
}
```

## Usage:
1. Use the "prepare-accountentry.sh" script in the root of the confluent-platform repo to make sure the topic "payments-accountentry" exists
2. Modify application-default.yml where needed, e.g. when your broker or schema registry node are on a different address.
3. Start the application to randomly create some accountentries for all accounts
```
mvn clean package spring-boot:run
```
4. Use the "check-accountentry.sh" script to check the payments-accountentry topic whether it was successful. Output like shown above should be visible.
